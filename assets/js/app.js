'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'myApp.config',
  'ngSanitize', 
  'ui.router',
  'myApp.filters',
  'myApp.directives',
  'myApp.services',
  'myApp.controllers'
])
  .run(['$rootScope', 'FBURL', function($rootScope, FBURL) {
    if( FBURL === 'https://INSTANCE.firebaseio.com' ) {
      // double-check that the app has been configured
      angular.element(document.body).html('<h1>Please configure app/js/config.js before running!</h1>');
      setTimeout(function() {
        angular.element(document.body).removeClass('hide');
      }, 250);
    }
    else {
      $rootScope.FBURL = FBURL;
    }
  }])
  .config(function($stateProvider, $urlRouterProvider) {
    var states = [{
        name: 'home',
        url: '/home',
        templateUrl: 'views/home.html',
       
    //     resolve: {
    //       user: function(User) {
    //           console.log('Verificando auntentificacion');
    //           return User.checkAuthentication();
    //       }
    //   }  
    },       
    {
        name: 'login',
        url: '/login',
        templateUrl: 'views/login.html',
      
    }
  ]
  $urlRouterProvider.otherwise('home');
  states.forEach(function(state) {
    $stateProvider.state(state);
  });
  });
  