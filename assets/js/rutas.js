var app = angular.module('app', ['ngSanitize', 'ui.router']);
app.config(function($stateProvider, $urlRouterProvider) {
    var states = [{
        name: 'home',
        url: '/home',
        templateUrl: 'views/home.html',
    //     resolve: {
    //       user: function(User) {
    //           console.log('Verificando auntentificacion');
    //           return User.checkAuthentication();
    //       }
    //   }  
    },       
    {
        name: 'login',
        url: '/login',
        templateUrl: 'views/login.html'
    }
  ]
  $urlRouterProvider.otherwise('home');
  states.forEach(function(state) {
    $stateProvider.state(state);
  });
  });
  app.run(function($state) {
    $state.go('home');
//   window.myAppErrorLog = [];
//   $state.defaultErrorHandler(function(error) {
//       // This is a naive example of how to silence the default error handler.
//       window.myAppErrorLog.push(error);
//       console.log('Error al autenticar, redirigiendo a login')
//       $state.go('login')
//   });

})