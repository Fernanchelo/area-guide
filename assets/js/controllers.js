'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
  .controller('HomeCtrl', ['$scope', '$location', 'syncData','$state', function($scope, $location, syncData, $state) {
    $scope.datos =[]; 
    $scope.get= function(coleccion){
      $scope.datos = syncData(coleccion);
    };


    $scope.margen="";
    $scope.formulario="";
    $scope.datos=[];
    $scope.dato={};

    $scope.sesion= function(type){
        if(type=='login'){
            $scope.registro=true;
            $scope.margen="mt-8";
            $scope.formulario="Iniciar Sesion";
        }else{
            $scope.registro=false;
            $scope.margen="mt-6";
            $scope.formulario="Registrate";
        }
        $state.go('login');
    };

  }])
  .directive("scroll", function ($window) {
    return function(scope, element, attrs) {
        angular.element($window).bind("scroll", function() {
             if (this.pageYOffset >= 75) {
                $('#one').addClass('Layout-aside-inner');
                $('#three').addClass('Layout-aside-inner');
                //  console.log('Scrolled below header.');
             } else {
                $('#one').removeClass('Layout-aside-inner');
                $('#three').removeClass('Layout-aside-inner');
                //  console.log('Header is in view.');
             }
             console.log(this.pageYOffset);
        });
    };
})
  .controller('NewUserCtrl', ['$scope', '$location', 'firebaseRef', function($scope, $location, firebaseRef) {
    $scope.createNewUser = function () {
      firebaseRef('users/'+ $scope.user.username).set({
        id: $scope.user.username,
        firstName: $scope.user.firstName,
        lastName: $scope.user.lastName,
        email: $scope.user.email,
        password: $scope.user.password
      });
      $location.path('/home');
    }
  }])
  .controller('UserCtrl', ['$scope', '$location', 'syncData', function($scope, $location, syncData) {
    $scope.users = syncData('users');

    $scope.createNewUser = function () {
      $location.path('/users/new');
    };
  }]);
