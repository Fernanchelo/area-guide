'use strict';

/* Controllers */

angular.module('myApp.controllers', []).controller('UserCtrl', ['$scope', '$location', 'syncData', function($scope, $location, syncData) {
  $scope.users = syncData('users');
  }]);
 